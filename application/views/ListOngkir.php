<div class="container">
    <?php if ($this->session->flashdata('flash')) : ?>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Mahasiswa <strong>berhasil</strong> <?= $this->session->flashdata('flash'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="row mt-3">
        <div class="col md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cek Kota Tujuan" name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Cari</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">List Tarif Ongkir</h3>
            <?php if (empty($cekOngkir)) : ?>
            <div class="alert alert-danger" role="alert">
                Data tidak ditemukan
            </div>
            <?php endif; ?>

            <table class="table mt-5">
                <thead>
                    <tr>
                        <th class="text-center" scope="col">KOTA TUJUAN</th>
                        <th class="text-center" scope="col">REGULER</th>
                        <th class="text-center" scope="col">EXTRA</th>
                        <th class="text-center" scope="col">KILAT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr><?php foreach ($cekOngkir as $cek) : ?>
                        <td class="text-center"><?= $cek['Kota']; ?></td>
                        <td class="text-center">Rp<?= $cek['Reguler']; ?></td>
                        <td class="text-center">Rp<?= $cek['Extra']; ?></td>
                        <td class="text-center">Rp<?= $cek['Kilat']; ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div> 