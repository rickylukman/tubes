<div class="container">    
    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">List Tarif Ongkir</h3>
            <?php if (empty($resi)) : ?>
            <div class="alert alert-danger" role="alert">
                Data tidak ditemukan
            </div>
            <?php endif; ?>

            <table class="table mt-5">
                <thead>
                    <tr>
                        <th class="text-center" scope="col">KOTA TUJUAN</th>
                        <th class="text-center" scope="col">REGULER</th>
                        <th class="text-center" scope="col">EXTRA</th>
                        <th class="text-center" scope="col">KILAT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr><?php foreach ($cekOngkir as $cek) : ?>
                        <td class="text-center"><?= $cek['Kota']; ?></td>
                        <td class="text-center">Rp<?= $cek['Reguler']; ?></td>
                        <td class="text-center">Rp<?= $cek['Extra']; ?></td>
                        <td class="text-center">Rp<?= $cek['Kilat']; ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>