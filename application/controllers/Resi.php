<?php
 // write your name and student id here
class Resi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model CO_model
		$this->load->model('Resi_model');
		//load library form validation
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['judul'] = 'Cek Resi';
		// $data['resi'] = $this->Resi_model->getAllInvoice();
		// if ($this->input->post('keyword')) {
		// 	$data['resi'] = $this->Resi_model->cariInvoice();
		// }
		$this->load->view('templates/header', $data);
		$this->load->view('resi/cekresi', $data);
		$this->load->view('templates/footer');
		$input = $this->input->post('keyword',true);
		$data['resi'] = $this->Resi_model->cariInvoice($input);
	}
}