<?php
 // write your name and student id here
class CekOngkir extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//load model CO_model
		$this->load->model('CO_model');
		//load library form validation
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['judul'] = 'List Biaya Ongkir';
		$data['cekOngkir'] = $this->CO_model->getAllTarif();
		if ($this->input->post('keyword')) {
			$data['cekOngkir'] = $this->CO_model->cariHargaTarif();
		}
		$this->load->view('templates/header', $data);
		$this->load->view('listongkir', $data);
		$this->load->view('templates/footer');
	}
}