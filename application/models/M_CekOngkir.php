<?php
 // write your name and student id here
class CO_model extends CI_model
{
	public function getAllTarif()
	{
		//use query builder to get data table "listongkir"
		$query = $this->db->get('listongkir');
		return $query->result_array();
  }
    
  public function cariHargaTarif()
	{
		$keyword = $this->input->post('keyword', true);
		//use query builder class to search data ongkir based on keyword "Kota Tujuan"
		$this->db->like('Kota', $keyword);
		//return data mahasiswa that has been searched
		$query = $this->db->get('listongkir');
		return $query->result_array();
	}
}
